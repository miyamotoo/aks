# AKS - Troubleshooting & Observability

## Troubleshoot your k8s deployments

### kubectl port-foward

* **Goal:** Connect to ports of pods running in a cluster by port forwarding local ports
* **Example** from [official documentation](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)
*  **Syntax:**

 ```
 kubectl port-forward -n <namespace> pod/<pod-name> <local-port>:<pod-port>
 kubectl port-forward -n <namespace> deployment/<deployment-name> <local-port>:<pod-port>
 kubectl port-forward -n <namespace> service/<service-name> <local-port>:<service-port>
 ```

### kubectl proxy

* **Goal:** creates a proxy server between your machine & the kubernetes API server. Can be used as alternative to kubectl port-forward
* **Example** from [official documentation](https://kubernetes.io/docs/tasks/access-kubernetes-api/http-proxy-access-api/)
*  **Syntax:**

 ```
1- kubectl proxy --port=<local-port>
 
2- http://localhost:<local-port>/api/
 
3- To proxy a service ,you can query the following resource : /api/v1/namespaces/<namespace-name>/services/<service-name>:<port>/proxy/ 
 ```
 
 *  **Example:** `curl http://localhost:8080/api/v1/namespaces/ingress-basic/services/web:8081/proxy/`

### kubectl exec
* **Goal:** exec a command in a container or get a shell in a container 
* **Example** from [official documentation](https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/)
*  **Syntax:**

 ```
kubectl exec –n <namespace> <pod-name> <command>
kubectl exec -it -n <namespace> <pod-name> <shell> 

 ```
 
 
### kubectl log
* **Goal:** Print the logs of a container in a cluster
* **Example** from [official documentation](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#interacting-with-running-pods)
*  **Syntax:**

 ```
kubectl logs –n <namespace> <pod-name>
kubectl logs –f <namespace> <pod-name> 

 ```


## Observability

Read more about [Azure monitor for containers](https://docs.microsoft.com/en-us/azure/azure-monitor/insights/container-insights-overview)

List of interesting tables in log analytics: [Container insights tables](https://docs.microsoft.com/en-us/azure/azure-monitor/insights/container-insights-log-search#container-records)

Queries based on KQL (Kusto Query Language): [KQL](https://docs.microsoft.com/en-us/azure/kusto/query/)

SQL to kusto cheat sheet: [SQL to KQL cheat sheet](https://docs.microsoft.com/en-us/azure/data-explorer/kusto/query/sqlcheatsheet)


Examples:

*  Draw timechart status of your containers 

```
KubePodInventory
| where Namespace == "<namespace>" and Name contains "<deployment-name>"
| summarize count() by  bin(TimeGenerated, 60s), ContainerStatus
| render timechart
```

*  Draw timechart CPU utilisation of your containers

```
KubePodInventory
| where isnotempty(Computer) // eliminate unscheduled pods
| where PodStatus in ('Running','Unknown') and Namespace == "<namespace>" and Name contains "<deployment-name>"
| summarize by bin(TimeGenerated, 1m), Computer, ClusterId, ContainerName, Namespace, PodUid
| project TimeGenerated, InstanceName = strcat(ClusterId, '/', ContainerName), PodUid
| join (
Perf
| where ObjectName == 'K8SContainer'
| where CounterName == 'cpuUsageNanoCores'
| summarize UsageValue = max(CounterValue) by bin(TimeGenerated, 1m), Computer, InstanceName, CounterName
| project-away CounterName
| join
(Perf
| where ObjectName == 'K8SContainer'
| where CounterName == 'cpuRequestNanoCores'
| summarize RequestValue = max(CounterValue) by bin(TimeGenerated, 1m), Computer, InstanceName, CounterName
| project-away CounterName
) on Computer, InstanceName, TimeGenerated
| project TimeGenerated = iif(isnotempty(TimeGenerated), TimeGenerated, TimeGenerated1), 
          Computer = iif(isnotempty(Computer), Computer, Computer1),
          InstanceName = iif(isnotempty(InstanceName), InstanceName, InstanceName1),
          UsageValue = iif(isnotempty(UsageValue), UsageValue, 0.0), 
          RequestValue = iif(isnotempty(RequestValue), RequestValue, 0.0)
| extend ConsumedValue = iif(UsageValue > RequestValue, UsageValue, RequestValue)
) on InstanceName, TimeGenerated
| summarize vCpuConsumedCores = sum(ConsumedValue) / 1000000000 by bin(TimeGenerated, 1m),PodUid
```

*  Draw timechart RAM utilisation of your containers

```
KubePodInventory
| where isnotempty(Computer) // eliminate unscheduled pods
| where PodStatus in ('Running','Unknown') and Namespace == "<namespace>" and Name contains "<deployment-name>"
| summarize by bin(TimeGenerated, 1m), Computer, ClusterId, ContainerName, Namespace, PodUid
| project TimeGenerated, InstanceName = strcat(ClusterId, '/', ContainerName), PodUid
| join (
Perf
| where ObjectName == 'K8SContainer'
| where CounterName == 'memoryRssBytes'
| summarize UsageValue = max(CounterValue) by bin(TimeGenerated, 1m), Computer, InstanceName, CounterName
| project-away CounterName
| join
(Perf
| where ObjectName == 'K8SContainer'
| where CounterName == 'memoryRequestBytes'
| summarize RequestValue = max(CounterValue) by bin(TimeGenerated, 1m), Computer, InstanceName, CounterName
| project-away CounterName
) on Computer, InstanceName, TimeGenerated
| project TimeGenerated = iif(isnotempty(TimeGenerated), TimeGenerated, TimeGenerated1), 
          Computer = iif(isnotempty(Computer), Computer, Computer1),
          InstanceName = iif(isnotempty(InstanceName), InstanceName, InstanceName1),
          UsageValue = iif(isnotempty(UsageValue), UsageValue, 0.0), 
          RequestValue = iif(isnotempty(RequestValue), RequestValue, 0.0)
| extend ConsumedValue = iif(UsageValue > RequestValue, UsageValue, RequestValue)
) on InstanceName, TimeGenerated
| summarize TotalMemoryConsumedMbytes = sum(ConsumedValue) /  1024 / 1024  by bin(TimeGenerated, 1m),PodUid
```


*  Stream logs of your containers

```
let ContainerIdList = KubePodInventory
| where Namespace == "<namespace>" and Name contains "<deployment-name>"
| distinct ContainerID;

ContainerLog
| where ContainerID in (ContainerIdList) and LogEntrySource == "stdout"
| project TimeGenerated, LogEntry
| order by TimeGenerated desc
```

